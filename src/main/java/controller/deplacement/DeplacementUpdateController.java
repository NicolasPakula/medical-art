package controller.deplacement;

import Entity.DeplacementEntity;
import model.DeplacementModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(name = "DeplacementUpdate", value = "/deplacementUpdate")
public class DeplacementUpdateController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DeplacementModel deplacementModel = new DeplacementModel(getServletContext());

        try {
            DeplacementEntity deplacement = deplacementModel.fetchById(Integer.parseInt(request.getParameter("id")));

            request.setAttribute("deplacement", deplacement);
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("WEB-INF/deplacement_update.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DeplacementModel deplacementModel = new DeplacementModel(getServletContext());

        try {
            deplacementModel.update(new DeplacementEntity(
                            Integer.parseInt(request.getParameter("id")),
                            Integer.parseInt(request.getParameter("patient_id")),
                            Integer.parseInt(request.getParameter("infirmiere_id")),
                            LocalDate.parse(request.getParameter("date")),
                            Integer.parseInt(request.getParameter("cout")),
                            "non"
                    )

            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.sendRedirect("deplacements");
    }
}
