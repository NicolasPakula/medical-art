<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
    <title>Modifier le déplacement</title>
</head>
<body>
<header>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Medical Art</a>
            <button
                    class="navbar-toggler"
                    type="button"
                    data-mdb-toggle="collapse"
                    data-mdb-target="#navbarToggler"
                    aria-controls="navbarToggler"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="patients">Patients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="infirmieres">Infirmières</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="deplacements">Déplacements</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<form class="col-xs-10 col-sm-8 col-md-6 col-lg-4 mt-4 offset-xs-1 offset-sm-2 offset-md-3 offset-lg-4"
      method="POST" action="deplacementCreate?id=${ deplacement.id }">

    <div class="form-outline mb-4">
        <label for="infirmiere_select">Infirmière</label>
        <select required name="infirmiere_id" id="infirmiere_select">
            <option selected value="">Choisissez une infirmière</option>
            <c:forEach items="${ infirmieres }" var="infirmiere">
                <option value="${ infirmiere.id }">${ infirmiere.nom} ${ infirmiere.prenom}</option>
            </c:forEach>
        </select>
    </div>

    <div class="form-outline mb-4">
        <label for="patient_select">Patient</label>
        <select required name="patient_id" id="patient_select">
            <option selected value="">Choisissez un patient</option>
            <c:forEach items="${ patients }" var="patient">
                <option value="${ patient.id }">${ patient.nom} ${ patient.prenom}</option>
            </c:forEach>
        </select>
    </div>

    <div class="form-outline mb-4">
        <div><label class="form-label" for="deplacement_date">Coup</label></div>
        <input required type="number" class="form-control" id="deplacement_cout" name="cout" value="">
    </div>

    <div class="form-outline mb-4">
        <div><label class="form-label" for="deplacement_date">Date</label></div>
        <input required type="date" class="form-control" id="deplacement_date" name="date" value="">
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-primary mb-4">Enregistrer</button>
    </div>
</form>

<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
</body>
</html>
