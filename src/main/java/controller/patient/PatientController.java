package controller.patient;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.AdresseEntity;
import Entity.InfirmiereEntity;
import Entity.PatientEntity;
import model.AdresseModel;
import model.InfirmiereModel;
import model.PatientModel;

/**
 * Servlet implementation class PatientController
 */
@WebServlet("/patient")
public class PatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PatientModel patientModel = new PatientModel(getServletContext());
		PatientEntity patient = null;
		AdresseModel adresseModel = new AdresseModel(getServletContext());
		AdresseEntity adresse = null;
		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
		InfirmiereEntity infirmiere = null;
		try {
			patient = patientModel.getById(Integer.parseInt(request.getParameter("id")));
			adresse = adresseModel.fetchById(patient.getAdresse_id());
			infirmiere =infirmiereModel.fetchById(patient.getInfirmiere_id());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("infirmiere", infirmiere);
		request.setAttribute("patient", patient);
		request.setAttribute("adresse", adresse);
		request.getRequestDispatcher("WEB-INF/patient.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
