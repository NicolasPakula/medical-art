package controller.infirmiere;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.AdresseEntity;
import Entity.InfirmiereEntity;
import Entity.PatientEntity;
import model.AdresseModel;
import model.InfirmiereModel;
import model.PatientModel;

/**
 * Servlet implementation class InfirmiereUpdate
 */
@WebServlet("/infirmiereUpdate")
public class InfirmiereUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InfirmiereUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Affichage la page de MAJ d'infirmiere dont l'id est pass� en param�tre dans l'URL
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdresseModel adresseModel = new AdresseModel(getServletContext());
		AdresseEntity adresse = null;
		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
		InfirmiereEntity infirmiere = null;
		try {
			infirmiere = infirmiereModel.fetchById(Integer.parseInt(request.getParameter("id")));
			adresse = adresseModel.fetchById(infirmiere.getAdresse_id());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("infirmiere", infirmiere);
		request.setAttribute("adresse", adresse);
		request.getRequestDispatcher("WEB-INF/infirmiere_update.jsp").forward(request, response);
	}	

	
	/**
	 * MAJ des infos de l'infirmiere dont l'id est pass� en param�tre dans l'URL
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Cree une adresse
		AdresseEntity adresse = new AdresseEntity(
				0,
				request.getParameter("numero"),
				request.getParameter("rue"),
				Integer.parseInt(request.getParameter("cp")),
				request.getParameter("ville"),
				"non"
				);
		AdresseModel model = new AdresseModel(getServletContext());
		int id = 0;
		try {
			id = model.create(adresse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Update un patient 
		InfirmiereEntity infirmiere = new InfirmiereEntity(
				Integer.parseInt(request.getParameter("infirmiere_id")),
				id,
				Integer.parseInt(request.getParameter("numeroProfessionnel")),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("telPro"),
				request.getParameter("telPerso"),
				"non"
				);
		InfirmiereModel modelP = new InfirmiereModel(getServletContext());
		try {
			modelP.update(infirmiere);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("infirmieres");
	}
}
