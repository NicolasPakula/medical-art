package controller.patient;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.AdresseEntity;
import Entity.InfirmiereEntity;
import Entity.PatientEntity;
import model.AdresseModel;
import model.InfirmiereModel;
import model.PatientModel;

/**
 * Servlet implementation class PatientUpdate
 */
@WebServlet("/patientUpdate")
public class PatientUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PatientUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PatientModel patientModel = new PatientModel(getServletContext());
		PatientEntity patient = null;
		AdresseModel adresseModel = new AdresseModel(getServletContext());
		AdresseEntity adresse = null;
		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
		List<InfirmiereEntity> infirmieres = new ArrayList<InfirmiereEntity>();
		try {
			patient = patientModel.getById(Integer.parseInt(request.getParameter("id")));
			adresse = adresseModel.fetchById(patient.getAdresse_id());
			infirmieres = infirmiereModel.fetchAll();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("infirmieres", infirmieres);
		request.setAttribute("patient", patient);
		request.setAttribute("adresse", adresse);
		request.getRequestDispatcher("WEB-INF/patient_update.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Cree une adresse
				AdresseEntity adresse = new AdresseEntity(
						0,
						request.getParameter("numero"),
						request.getParameter("rue"),
						Integer.parseInt(request.getParameter("cp")),
						request.getParameter("ville"),
						"non"
						);
				AdresseModel model = new AdresseModel(getServletContext());
				int id = 0;
				try {
					id = model.create(adresse);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//Update un patient 
				PatientEntity patient = new PatientEntity(
						Integer.parseInt(request.getParameter("patient_id")),
						id,
						Integer.parseInt(request.getParameter("infirmiere_id")),
						request.getParameter("nom"),
						request.getParameter("prenom"),
						LocalDate.parse(request.getParameter("dateDeNaissance")),
						request.getParameter("sexe"),
						Integer.parseInt(request.getParameter("numeroSecuriteSocial")),
						"non"
						);
				PatientModel modelP = new PatientModel(getServletContext());
				try {
					modelP.update(patient);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				response.sendRedirect("patients");
	}

}
