package controller.infirmiere;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.AdresseEntity;
import Entity.InfirmiereEntity;
import model.AdresseModel;
import model.InfirmiereModel;

/**
 * Servlet implementation class InfirmiereCreate
 */
@WebServlet("/infirmiereCreate")
public class InfirmiereCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfirmiereCreate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Affiche la page de cr�ation d'infirmiere
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.getRequestDispatcher("WEB-INF/infirmiere_create.jsp").forward(request, response);
	}
	
	
	/**
	 * Cr�ation d'une infirmi�re
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Cr�er une adresse
		AdresseEntity adresse = new AdresseEntity(
				0,
				request.getParameter("numero"),
				request.getParameter("rue"),
				Integer.parseInt(request.getParameter("cp")),
				request.getParameter("ville"),
				"non"
				);
		AdresseModel model = new AdresseModel(getServletContext());
		int id = 0;
		try {
			id = model.create(adresse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// Cr�er une infirmiere
		InfirmiereEntity infirmiere = new InfirmiereEntity(
				0,
				id,
				Integer.parseInt(request.getParameter("numeroProfessionnel")),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("telPro"),
				request.getParameter("telPerso;"),
				"non"
				);
		
		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());

		try {
			infirmiereModel.create(infirmiere);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		response.sendRedirect("infirmieres");
	}
	
}
