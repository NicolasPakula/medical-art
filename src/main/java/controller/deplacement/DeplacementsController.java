package controller.deplacement;

import Entity.DeplacementEntity;
import model.DeplacementModel;
import model.InfirmiereModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Deplacements", value = "/deplacements")
public class DeplacementsController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DeplacementModel deplacementModel = new DeplacementModel(getServletContext());
        List<DeplacementEntity> deplacements;

        String userType = (request.getParameter("userType") == null)? "" : request.getParameter("userType");
        String id = request.getParameter("id");

        try {
            switch (userType.toLowerCase()) {
                case "infirmiere":
                    deplacements = deplacementModel.fetchByInfirmiereId(Integer.parseInt(id));
                    break;
                case "patient":
                    deplacements = deplacementModel.fetchByPatientId(Integer.parseInt(id));
                    break;
                default:
                    deplacements = deplacementModel.fetchAll();
            }

            request.setAttribute("deplacements", deplacements);
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("WEB-INF/deplacements.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
