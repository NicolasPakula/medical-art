package model;
import Entity.PatientEntity;

import javax.servlet.ServletContext;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.time.LocalDate;
public class PatientModel extends AccessDB {

	/**
	 *
	 * @param context
	 */
	public PatientModel(ServletContext context) {
		super(context);
	}


	public List<PatientEntity> fetchAll() throws Exception {
		
		List<PatientEntity> patients = new ArrayList<>();
		
		Statement statement = this.connexion().createStatement();
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM patient where isSuppr='non' ");
			while(result.next()) {
				
				patients.add(new PatientEntity(
						result.getInt("id"),
						result.getInt("adresse_id"),
						result.getInt("infirmiere_id"),
						result.getString("nom"),
						result.getString("prenom"),
						(LocalDate)result.getObject("dateDeNaissance", LocalDate.class ),
						result.getString("sexe"),
						result.getInt("numeroSecuriteSocial"),
						result.getString("isSuppr")
						));			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
		return patients;
	}

	/**
	 * Supprime un patient
	 * @param id
	 * @throws Exception
	 */
	public void deleteById(int id) throws Exception{
		try {
			Statement statement = this.connexion().createStatement();
			statement.executeUpdate("UPDATE patient SET isSuppr = 'oui' WHERE id="+id);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}

	/**
	 * Crée un patient
	 * @param patient
	 * @throws Exception
	 */
	public void create(PatientEntity patient) throws Exception {
		try {
			PreparedStatement statement = this.connexion().prepareStatement("INSERT INTO patient (adresse_id,infirmiere_id,nom,prenom,dateDeNaissance,sexe,numeroSecuriteSocial,isSuppr)"
					+ " VALUES (?,?,?,?,?,?,?,?)");
			statement.setInt(1, patient.getAdresse_id());
			statement.setInt(2, patient.getInfirmiere_id());
			statement.setString(3, patient.getNom());
			statement.setString(4, patient.getPrenom());
			statement.setObject(5, patient.getDateDeNaissance());
			statement.setString(6, patient.getSexe());
			statement.setInt(7, patient.getNumeroSecuriteSocial());
			statement.setString(8, patient.getIsSuppr());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}


	public PatientEntity getById(int id) throws Exception {
		// TODO Auto-generated method stub
		PatientEntity patient = null;
		
		Statement statement = this.connexion().createStatement();
		ResultSet result;
		
		try {
			result = statement.executeQuery("SELECT * FROM patient where isSuppr='non' AND id='"+id+"'");
			while(result.next()) {
				
				patient = new PatientEntity(
						result.getInt("id"),
						result.getInt("adresse_id"),
						result.getInt("infirmiere_id"),
						result.getString("nom"),
						result.getString("prenom"),
						(LocalDate)result.getObject("dateDeNaissance", LocalDate.class ),
						result.getString("sexe"),
						result.getInt("numeroSecuriteSocial"),
						result.getString("isSuppr")
						);			
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		return patient;
	}


	public void update(PatientEntity patient) throws Exception {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = this.connexion().prepareStatement("UPDATE patient SET adresse_id=?,infirmiere_id=?,nom=?"
					+ ",prenom=?,dateDeNaissance=?,sexe=?,numeroSecuriteSocial=?,isSuppr=? where id=?");
				
			statement.setInt(1, patient.getAdresse_id());
			statement.setInt(2, patient.getInfirmiere_id());
			statement.setString(3, patient.getNom());
			statement.setString(4, patient.getPrenom());
			statement.setObject(5, patient.getDateDeNaissance());
			statement.setString(6, patient.getSexe());
			statement.setInt(7, patient.getNumeroSecuriteSocial());
			statement.setString(8, patient.getIsSuppr());
			statement.setInt(9, patient.getId());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
	}
}
