package controller.deplacement;

import Entity.DeplacementEntity;
import Entity.InfirmiereEntity;
import Entity.PatientEntity;
import model.DeplacementModel;
import model.InfirmiereModel;
import model.PatientModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "DeplacementCreate", value = "/deplacementCreate")
public class DeplacementCreateController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
        PatientModel patientModel = new PatientModel(getServletContext());

        try {
            List<InfirmiereEntity> infirmieres = infirmiereModel.fetchAll();
            request.setAttribute("infirmieres", infirmieres);

            List<PatientEntity> patients = patientModel.fetchAll();
            request.setAttribute("patients", patients);
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.getRequestDispatcher("WEB-INF/deplacement_create.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DeplacementModel deplacementModel = new DeplacementModel(getServletContext());

        try {
            deplacementModel.create(new DeplacementEntity(
                    0,
                    Integer.parseInt(request.getParameter("patient_id")),
                    Integer.parseInt(request.getParameter("infirmiere_id")),
                    LocalDate.parse(request.getParameter("date")),
                    Integer.parseInt(request.getParameter("cout")),
                    "non"
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.sendRedirect("deplacements");
    }

}
