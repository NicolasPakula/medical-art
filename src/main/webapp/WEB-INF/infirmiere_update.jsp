<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Infirmiere Update</title>
</head>
<body>
<header>

	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">Medical Art</a>
			<button
					class="navbar-toggler"
					type="button"
					data-mdb-toggle="collapse"
					data-mdb-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="patients">Patients</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="infirmieres">Infirmières</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="deplacements">Déplacements</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>
<div class="container" style="display: flex;justify-content: center;">

	<form method="POST" action="infirmiereUpdate">
		<input type="hidden" name="infirmiere_id" value="${infirmiere.id}">
		<div class="mb-3">
		<label class="form-label"> Nom
			<input type="text" name="nom" value="${infirmiere.nom}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> Prenom
			<input type="text" name="prenom" value="${infirmiere.prenom}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> numero pro
			<input type="number" name="numeroProfessionnel" value="${infirmiere.numeroProfessionnel}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> Tel Pro
			<input type="text" name="telPro" value="${infirmiere.telPro}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> Tel Perso
			<input type="text" name="telPerso" value="${infirmiere.telPerso}" class="form-control">
		</label>
		</div>
		<input type="hidden" name="adresse_id" value="${adresse.id}">
		<div class="mb-3">
		<label class="form-label"> numero de rue
			<input type="number" name="numero" value="${adresse.numero}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> rue
			<input type="text" name="rue" value="${adresse.rue}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> code postal
			<input type="number" name="cp" value="${adresse.cp}" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> ville
			<input type="text" name="ville" value="${adresse.ville}" class="form-control">
		</label>
		</div>
		<input type="submit" value="Submit">
	</form>
</div>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>