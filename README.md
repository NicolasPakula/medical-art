# PROJET SERVLET MEDICAL

## Préparatifs

### Modifier la BDD

```
ALTER TABLE patient ADD isSuppr ENUM('non', 'oui') NOT NULL;
ALTER TABLE infirmiere ADD isSuppr ENUM('non', 'oui') NOT NULL;
ALTER TABLE deplacement ADD isSuppr ENUM('non', 'oui') NOT NULL;
ALTER TABLE adresse ADD isSuppr ENUM('non', 'oui') NOT NULL;
```

### Modifier les crédentials de la BDD

Vous devez rajouter un fichier config.properties qui aura la forme suivante en fonction de vos crédentials :

```bash
db.username=root
db.password=toto
db.url=jdbc:mysql://localhost:3307/medical
```

Le fichier devra être positionné comme montré ci-dessous :
```bash
|──src
│   └───main
│       ├───java
│       └───webapp
│           ├───META-INF
│           └───WEB-INF
│               └───config
│                   └───config.properties <--- Insérer ici
```