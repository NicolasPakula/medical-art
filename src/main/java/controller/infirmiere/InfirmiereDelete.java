package controller.infirmiere;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.InfirmiereModel;

/**
 * Servlet implementation class InfirmiereDelete
 */
@WebServlet("/infirmiereDelete")
public class InfirmiereDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfirmiereDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Supprime l'infirmiere dont l'id est pass� en param�tre dans l'URL
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
		try {
			infirmiereModel.deleteById(Integer.parseInt(request.getParameter("id")));
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect("infirmieres");
	}
}
