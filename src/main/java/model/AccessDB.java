package model;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class AccessDB {

	private String url;
	private String username;
	private String password;
	private ServletContext context;

	/**
	 *
	 * @param context
	 */
	public AccessDB(ServletContext context) {
		try(InputStream input = context.getResourceAsStream("/WEB-INF/config/config.properties")) {
			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			this.url = prop.getProperty("db.url");
			this.username = prop.getProperty("db.username");
			this.password = prop.getProperty("db.password");
			this.context = context;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws Exception 
	*/
	
	public Connection connexion() throws Exception {
		
		Connection con = null;
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		con = DriverManager.getConnection(this.url, this.username, this.password);
		
		return con;
	}

	protected ServletContext getContext() {
		return this.context;
	}
}
