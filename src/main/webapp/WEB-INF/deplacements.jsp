<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />
    <title>Déplacements</title>
</head>
<body>
<header>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
        <div class="container-fluid">
            <a class="navbar-brand" href="/">Medical Art</a>
            <button
                    class="navbar-toggler"
                    type="button"
                    data-mdb-toggle="collapse"
                    data-mdb-target="#navbarToggler"
                    aria-controls="navbarToggler"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
            >
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="patients">Patients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="infirmieres">Infirmières</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="deplacements">Déplacements</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>


    <div class="col-xs-12 col-md-10 col-lg-8 offset-lg-2 offset-md-1 mt-4">
        <table class="table border table-hover align-middle">
            <thead class="table-dark">
                <tr>
                    <th class="text-center"> # </th>
                    <th> Infirmière </th>
                    <th> Patient </th>
                    <th> Date </th>
                    <th> Cout </th>
                    <th> Actions </th>
                </tr>
            </thead>
            <tbody>
            <c:set var="increment" value="1" scope="page" />
            <c:forEach items="${ deplacements }" var="deplacement">
                <tr>
                    <td class="border text-center">${ increment }</td>
                    <td>${ deplacement.infirmiere.nom } ${ deplacement.infirmiere.prenom }</td>
                    <td>${ deplacement.patient.nom } ${ deplacement.patient.prenom }</td>
                    <td>${ deplacement.date }</td>
                    <td>${ deplacement.cout }€</td>
                    <td>
                        <a class="btn btn-secondary px-3 me-2" href="deplacementUpdate?id=${ deplacement.id }" role="button"><i class="fas fa-pen"></i></a>
                        <a class="btn btn-danger px-3" href="deplacementDelete?id=${ deplacement.id }" role="button"><i class="fas fa-trash-alt"></i></a>
                    </td>
                </tr>

                <c:set var="increment" value="${increment + 1}" scope="page"/>
            </c:forEach>
            </tbody>
        </table>
        <a class="btn btn-primary" href="deplacementCreate" role="button">Nouveau</a>
    </div>

<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
</body>
</html>
