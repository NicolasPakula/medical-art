package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import Entity.InfirmiereEntity;
import Entity.PatientEntity;

public class InfirmiereModel extends AccessDB{

	public InfirmiereModel(ServletContext context) {
		super(context);
	}


	public List<InfirmiereEntity> fetchAll() throws Exception{

		List<InfirmiereEntity> infirmieres = new ArrayList<>();

		Statement statement = this.connexion().createStatement();
		ResultSet result;

		try {
			result = statement.executeQuery("SELECT * FROM infirmiere WHERE isSuppr='non'");
			while(result.next()) {

				infirmieres.add(new InfirmiereEntity(
						result.getInt("id"),
						result.getInt("adresse_id"),
						result.getInt("numeroProfessionnel"),
						result.getString("nom"),
						result.getString("prenom"),
						result.getString("telPro"),
						result.getString("telPerso"),
						result.getString("isSuppr")
						));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			this.connexion().close();
		}

		return infirmieres;
	}

	/**
	 * Retourne l'infirmiere qui correspond à l'id en paramètre
	 * @param id
	 * @return InfirmiereEntity
	 * @throws Exception
	 */
	public InfirmiereEntity fetchById(int id) throws Exception {
		InfirmiereEntity infirmiere = new InfirmiereEntity();
		try {
			Statement statement = this.connexion().createStatement();

			ResultSet result = statement.executeQuery("SELECT * FROM infirmiere WHERE id='"+id+"' AND isSuppr='non'");

			if (result.next()) {
				infirmiere.setId(result.getInt("id")); 
				infirmiere.setAdresse_id(result.getInt("adresse_id"));
				infirmiere.setNumeroProfessionnel(result.getInt("numeroProfessionnel"));
				infirmiere.setNom(result.getString("nom"));
				infirmiere.setPrenom(result.getString("prenom"));
				infirmiere.setTelPro(result.getString("telPro"));
				infirmiere.setTelPerso(result.getString("telPerso"));
				infirmiere.setIsSuppr(result.getString("isSuppr"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		return infirmiere;
	}


	public void deleteById(int id) throws Exception {
		try {
			Statement statement = this.connexion().createStatement();
			statement.executeUpdate("UPDATE infirmiere SET isSuppr ='oui'WHERE id="+id);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
	}
	
	public void create(InfirmiereEntity infirmiere) throws Exception {
		try {
			PreparedStatement statement = this.connexion().prepareStatement("INSERT INTO infirmiere (adresse_id, numeroProfessionnel, nom, prenom, telPro, telPerso, isSuppr)"
					+ "VALUES (?,?,?,?,?,?,?)");
			statement.setInt(1, infirmiere.getAdresse_id());
			statement.setInt(2, infirmiere.getNumeroProfessionnel());
			statement.setString(3, infirmiere.getNom());
			statement.setString(4, infirmiere.getPrenom());
			statement.setString(5, infirmiere.getTelPro());
			statement.setString(6, infirmiere.getTelPerso());
			statement.setString(7, infirmiere.getIsSuppr());
			statement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
				this.connexion().close();
		}
		
	}

	public void update(InfirmiereEntity infirmiere) throws Exception {
		// TODO Auto-generated method stub
		try {
			PreparedStatement statement = this.connexion().prepareStatement("UPDATE infirmiere SET adresse_id=?,numeroProfessionnel=?,nom=?"
					+ ",prenom=?,telPro=?,telPerso=?,isSuppr=? where id=?");
				
			statement.setInt(1, infirmiere.getAdresse_id());
			statement.setInt(2, infirmiere.getNumeroProfessionnel());
			statement.setString(3, infirmiere.getNom());
			statement.setString(4, infirmiere.getPrenom());
			statement.setString(5, infirmiere.getTelPro());
			statement.setString(6, infirmiere.getTelPerso());
			statement.setString(7, infirmiere.getIsSuppr());
			statement.setInt(8, infirmiere.getId());
			statement.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			this.connexion().close();
		}
		
	}

}
