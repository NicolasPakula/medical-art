package controller.infirmiere;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.InfirmiereEntity;
import model.InfirmiereModel;

/**
 * Servlet implementation class InfirmieresController
 */
@WebServlet("/infirmieres")
public class InfirmieresController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfirmieresController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Affiche la liste de toutes les infirmieres (celles indiqu�es comme "non supprim�")
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		InfirmiereModel connex = new InfirmiereModel(getServletContext());
		try {
			List<InfirmiereEntity> infirmieres = connex.fetchAll();
			request.setAttribute("infirmieres", infirmieres);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.getRequestDispatcher("WEB-INF/infirmieres.jsp").forward(request, response);
	}
}
