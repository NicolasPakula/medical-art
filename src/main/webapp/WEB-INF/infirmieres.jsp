<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<!-- Font Awesome -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
	rel="stylesheet" />
<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
	rel="stylesheet" />
<!-- MDB -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
	rel="stylesheet" />
<title>Liste des infirmi�res</title>
</head>
<body>

<header>

	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">Medical Art</a>
			<button
					class="navbar-toggler"
					type="button"
					data-mdb-toggle="collapse"
					data-mdb-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="patients">Patients</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="infirmieres">Infirmi�res</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="deplacements">D�placements</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

	<div class="col-xs-12 col-md-10 offset-md-1 mt-4">
		<table class="table border table-hover align-middle">
			<thead class="table-dark">
				<tr>
					<th>Num�ro professionnel</th>
					<th>Nom</th>
					<th>Pr�nom</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<c:forEach items="${ infirmieres }" var="infirmiere">
				<tbody>
					<tr>
						<td>${infirmiere.numeroProfessionnel }</td>
						<td>${infirmiere.nom }</td>
						<td>${infirmiere.prenom }</td>
						<td><a href="infirmiere?id=${infirmiere.id }"
							class="btn btn-primary btn-sm">Details</a></td>
						<td> <a href="deplacements?id=${ infirmiere.id }&userType=infirmiere"
							class="btn btn-primary btn-sm"> Voir Deplacements </a> </td>
						<td><a href="infirmiereUpdate?id=${ infirmiere.id }"
							class="btn btn-secondary btn-sm">Modifier</a></td>
						<td><a href="infirmiereDelete?id=${ infirmiere.id }"
							class="btn btn-danger btn-sm">X</a></td>
					</tr>
				</tbody>
			</c:forEach>
		</table>
		<a href="infirmiereCreate" class="btn btn-primary">Nouveau</a>
	</div>


	<!-- MDB -->
	<script type="text/javascript"
		src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
</body>
</html>