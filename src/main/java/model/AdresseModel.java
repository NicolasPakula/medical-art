package model;

import Entity.AdresseEntity;

import javax.servlet.ServletContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AdresseModel extends AccessDB {

    public AdresseModel(ServletContext context) {
        super(context);
    }

    /**
     * fetch an adresse from the table adresse using is own id
     *
     * @param id
     * @return
     * @throws Exception
     */
    public AdresseEntity fetchById(int id) throws Exception {

        AdresseEntity adresse = null;
        Statement statement = this.connexion().createStatement();
        ResultSet result;

        try {
            result = statement.executeQuery("SELECT * FROM adresse WHERE id=" + id);
            result.next();
            adresse = new AdresseEntity(
                    result.getInt("id"),
                    result.getString("numero"),
                    result.getString("rue"),
                    result.getInt("cp"),
                    result.getString("ville"),
                    result.getString("isSuppr")
            );
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connexion().close();
        }
        return adresse;
    }

    /**
     * Check if an adresse already exists
     *
     * @param numero
     * @param rue
     * @param cp
     * @param ville
     * @param isSuppr
     * @return the id of the adresse if it exists or -1 if it does not
     * @throws Exception
     */
    public int checkId(String numero, String rue, int cp, String ville, String isSuppr) throws Exception {
        int id = -1;
        Statement statement = this.connexion().createStatement();
        ResultSet result;

        try {

            result = statement.executeQuery("SELECT id FROM adresse WHERE numero='" + numero + "' AND rue='" + rue + "' AND cp='" + cp + "'AND ville='" + ville + "' AND isSuppr='" + isSuppr +"'");
            while(result.next()) {
                if (result.getInt("id") != 0) {
                    id = result.getInt("id");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connexion().close();
        }
        return id;
    }

    /**
     * create an adresse and return the status
     *
     * @param adresse
     * @return
     */
    public int create(AdresseEntity adresse) throws Exception {
        int id = checkId(adresse.getNumero(), adresse.getRue(), adresse.getCp(), adresse.getVille(), adresse.getIsSuppr());
        if (id == -1) {
            try {
                PreparedStatement preparedStatement = this.connexion().prepareStatement(
                        "INSERT INTO adresse(numero,rue,cp,ville,isSuppr) VALUES (?,?,?,?,?)");
                preparedStatement.setString(1, adresse.getNumero());
                preparedStatement.setString(2, adresse.getRue());
                preparedStatement.setInt(3, adresse.getCp());
                preparedStatement.setString(4, adresse.getVille());
                preparedStatement.setString(5, adresse.getIsSuppr());
                preparedStatement.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                this.connexion().close();
            }
        }
        id = checkId(adresse.getNumero(), adresse.getRue(), adresse.getCp(), adresse.getVille(), adresse.getIsSuppr());
        return id;
    }

    /**
     * Update an adresse
     *
     * @param adresse
     * @throws Exception
     */
    public void update(AdresseEntity adresse) throws Exception {
        try {
            PreparedStatement preparedStatement = this.connexion().prepareStatement(
                    "update adresse set numero=?,rue=?,cp=?,ville=?,isSuppr=? where id=?");
            preparedStatement.setString(1, adresse.getNumero());
            preparedStatement.setString(2, adresse.getRue());
            preparedStatement.setInt(3, adresse.getCp());
            preparedStatement.setString(4, adresse.getVille());
            preparedStatement.setString(5, adresse.getIsSuppr());
            preparedStatement.setInt(6, adresse.getId());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connexion().close();
        }
    }

    /**
     * delete an adresse
     *passe la valeur isSuppr à "oui"
     * @param id
     * @throws Exception
     */
    public void deleteById(int id) throws Exception {
        try {
            Statement statement = this.connexion().createStatement();
            statement.executeUpdate("Update adresse set isSuppr = 'oui' where id=" + id);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connexion().close();
        }
    }
}
