package Entity;

import java.time.LocalDate;

public class DeplacementEntity {

    private int id;
    private int patient_id;
    private PatientEntity patient;
    private int infirmiere_id;
    private InfirmiereEntity infirmiere;
    private LocalDate date;
    private double cout;
    private String isSuppr;


    public DeplacementEntity(int id, int patient_id, int infirmiere_id, LocalDate date, double cout, String isSuppr) {
        this.id = id;
        this.patient_id = patient_id;
        this.infirmiere_id = infirmiere_id;
        this.date = date;
        this.cout = cout;
        this.isSuppr = isSuppr;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the patient_id
     */
    public int getPatient_id() {
        return patient_id;
    }

    /**
     * @param patient_id the patient_id to set
     */
    public void setPatient_id(int patient_id) {
        this.patient_id = patient_id;
    }

    /**
     * @return the patient
     */
    public PatientEntity getPatient() {
        return patient;
    }

    /**
     * @param patient the patient to set
     */
    public void setPatient(PatientEntity patient) {
        this.patient = patient;
    }

    /**
     * @return the infirmiere_id
     */
    public int getInfirmiere_id() {
        return infirmiere_id;
    }

    /**
     * @param infirmiere_id the infirmiere_id to set
     */
    public void setInfirmiere_id(int infirmiere_id) {
        this.infirmiere_id = infirmiere_id;
    }

    /**
     * @return the infirmiere
     */
    public InfirmiereEntity getInfirmiere() {
        return infirmiere;
    }

    /**
     * @param infirmiere the infirmiere to set
     */
    public void setInfirmiere(InfirmiereEntity infirmiere) {
        this.infirmiere = infirmiere;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the cout
     */
    public double getCout() {
        return cout;
    }

    /**
     * @param cout the cout to set
     */
    public void setCout(double cout) {
        this.cout = cout;
    }

    /**
     * @return the isSuppr
     */
    public String getIsSuppr() {
        return isSuppr;
    }

    /**
     * @param isSuppr the isSuppr to set
     */
    public void setIsSuppr(String isSuppr) {
        this.isSuppr = isSuppr;
    }
}
