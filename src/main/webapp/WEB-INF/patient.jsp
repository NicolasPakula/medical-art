<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css" rel="stylesheet" />


<title>Patient</title>
</head>
<body>
<header>

	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">Medical Art</a>
			<button
					class="navbar-toggler"
					type="button"
					data-mdb-toggle="collapse"
					data-mdb-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="patients">Patients</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="infirmieres">Infirmières</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="deplacements">Déplacements</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

<div class="col-xs-12 col-md-10 col-lg-8 offset-lg-2 offset-md-1 mt-4">
		<table class="table border table-hover align-middle">
			<thead class="table-dark">
			<tr>
				<th class="text-center"> Id patient: ${patient.id} </th>
			</tr>
			</thead>
			<tbody>
				<tr><td>${patient.nom}</td></tr>
				<tr><td>${patient.prenom}</td></tr>
				<tr><td>${patient.dateDeNaissance}</td></tr>
				<tr><td>${patient.sexe}</td></tr>
				<tr><td>${patient.numeroSecuriteSocial}</td></tr>
				<tr><td>${adresse.numero} ${adresse.rue} ${adresse.cp} ${adresse.ville}</td></tr>
			</tbody>
			</table>
		</div>
</body>
</html>