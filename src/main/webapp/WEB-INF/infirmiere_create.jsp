<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<!-- Font Awesome -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>
<!-- MDB -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
  rel="stylesheet"
/>
<title>Infirmiere Create</title>
</head>
<body>
<header>

	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">Medical Art</a>
			<button
					class="navbar-toggler"
					type="button"
					data-mdb-toggle="collapse"
					data-mdb-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="patients">Patients</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="infirmieres">Infirmières</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="deplacements">Déplacements</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

	<form method="POST" action="infirmiereCreate">
		<label> Numero Professionnel
			<input type="text" name="numeroProfessionnel">
		</label>
		<label> Nom
			<input type="text" name="nom">
		</label>
		<label> Prenom
			<input type="text" name="prenom">
		</label>
		<label> Telephone Professionnel
			<input type="text" name="telPro">
		</label>
		<label> Telephone Personnel
			<input type="text" name="telPerso">
		</label>


		<label> numero de rue
			<input type="number" name="numero">
		</label>
		<label> rue
			<input type="text" name="rue">
		</label>
		<label> code postal
			<input type="number" name="cp">
		</label>
		<label> ville
			<input type="text" name="ville">
		</label>
		<input type="submit" value="Submit">
	</form>
	

<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"
></script>
</body>
</html>