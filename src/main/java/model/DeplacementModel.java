package model;

import Entity.DeplacementEntity;
import Entity.InfirmiereEntity;

import javax.servlet.ServletContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DeplacementModel extends AccessDB {

    public DeplacementModel(ServletContext context) {
        super(context);
    }


    /**
     * Récupère tous les déplacements
     * @return
     * @throws Exception
     */
    public List<DeplacementEntity> fetchAll() throws Exception {
        return fetchByPatientOrInfirmiereSpecific("");
    }

    /**
     * Récupère une liste des déplacements d'un patient
     * @param patientId
     * @return
     * @throws Exception
     */
    public List<DeplacementEntity> fetchByPatientId(int patientId) throws Exception {
        String specificRequestPart = " AND patient_id="+patientId;
        return fetchByPatientOrInfirmiereSpecific(specificRequestPart);
    }

    /**
     * Récupère une liste des déplacements d'une infirmière
     * @param infirmiereId
     * @return
     * @throws Exception
     */
    public List<DeplacementEntity> fetchByInfirmiereId(int infirmiereId) throws Exception {
        String specificRequestPart = " AND infirmiere_id="+infirmiereId;
        return fetchByPatientOrInfirmiereSpecific(specificRequestPart);
    }

    /**
     * Récupère une liste des déplacements d'un patient ou d'une infirmière
     * selon un critère spécifique
     * @param specificRequestPart
     * @return
     * @throws Exception
     */
    private List<DeplacementEntity> fetchByPatientOrInfirmiereSpecific(String specificRequestPart) throws Exception {
        List<DeplacementEntity> deplacements = new ArrayList<>();

        PatientModel patientModel = new PatientModel(getContext());
        InfirmiereModel infirmiereModel = new InfirmiereModel(getContext());

        Statement statement = this.connexion().createStatement();
        ResultSet result;

        try {
            result = statement.executeQuery("SELECT * FROM deplacement WHERE isSuppr='non' " + specificRequestPart);
            while(result.next()) {

                DeplacementEntity deplacement = new DeplacementEntity(
                        result.getInt("id"),
                        result.getInt("patient_id"),
                        result.getInt("infirmiere_id"),
                        result.getObject("date", LocalDate.class),
                        result.getInt("cout"),
                        result.getString("isSuppr")
                );

                deplacement.setPatient(patientModel.getById(deplacement.getPatient_id()));
                deplacement.setInfirmiere(infirmiereModel.fetchById(deplacement.getInfirmiere_id()));

                deplacements.add(deplacement);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return deplacements;
    }

    /**
     * Récupère un déplacement
     * @param deplacementId
     * @return
     * @throws Exception
     */
    public DeplacementEntity fetchById(int deplacementId) throws Exception {
        PatientModel patientModel = new PatientModel(getContext());
        InfirmiereModel infirmiereModel = new InfirmiereModel(getContext());

        DeplacementEntity deplacement;

        Statement statement = this.connexion().createStatement();
        ResultSet result;

        try {
            result = statement.executeQuery("SELECT * FROM deplacement WHERE id=" + deplacementId);
            if (result.next()) {
                deplacement = new DeplacementEntity(
                        result.getInt("id"),
                        result.getInt("patient_id"),
                        result.getInt("infirmiere_id"),
                        result.getObject("date", LocalDate.class),
                        result.getInt("cout"),
                        result.getString("isSuppr")
                );

                deplacement.setPatient(patientModel.getById(deplacement.getPatient_id()));
                deplacement.setInfirmiere(infirmiereModel.fetchById(deplacement.getInfirmiere_id()));

                return deplacement;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Crée un déplacement
     * @return l'id du nouveau déplacement
     */
    public int create(DeplacementEntity deplacement) {
        String request =
                "INSERT INTO deplacement(patient_id, infirmiere_id, date, cout, isSuppr) values (?,?,?,?,?)";

        try (PreparedStatement statement = this.connexion().prepareStatement(request, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt(1, deplacement.getPatient_id());
            statement.setInt(2, deplacement.getInfirmiere_id());
            statement.setObject(3, deplacement.getDate());
            statement.setDouble(4, deplacement.getCout());
            statement.setString(5, deplacement.getIsSuppr());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating deplacement failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
                else {
                    throw new SQLException("Creating deplacement failed, no ID obtained.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Modifie un déplacement
     * @param deplacement
     */
    public void update(DeplacementEntity deplacement) {
        String request =
            "UPDATE deplacement SET patient_id=?, infirmiere_id=?, date=?, cout=?, isSuppr=? WHERE id=" + deplacement.getId();

        try (PreparedStatement statement = this.connexion().prepareStatement(request)) {
            statement.setInt(1, deplacement.getPatient_id());
            statement.setInt(2, deplacement.getInfirmiere_id());
            statement.setObject(3, deplacement.getDate());
            statement.setDouble(4, deplacement.getCout());
            statement.setString(5, deplacement.getIsSuppr());

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Updating deplacement failed, no rows affected.");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Supprime un déplacement (passe la valeur isSuppr à 'oui')
     * @param id
     */
    public void delete(int id) {
        String request = "UPDATE deplacement SET isSuppr='oui' WHERE id=?";

        try (PreparedStatement statement = this.connexion().prepareStatement(request)) {
            statement.setInt(1, id);

            int affectedRows = statement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Deletion of deplacement failed, no rows affected.");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
