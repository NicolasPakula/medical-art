package Entity;

public class InfirmiereEntity {

	private int id;
	private int adresse_id;
	private int numeroProfessionnel;
	private String nom;
	private String prenom;
	private String telPro;
	private String telPerso;
	private String isSuppr;
	
	public InfirmiereEntity(int id, int adresse_id, int numeroProfessionnel, String nom, String prenom, String telPro,
			String telPerso, String isSuppr) {
		super();
		this.id = id;
		this.adresse_id = adresse_id;
		this.numeroProfessionnel = numeroProfessionnel;
		this.nom = nom;
		this.prenom = prenom;
		this.telPro = telPro;
		this.telPerso = telPerso;
		this.isSuppr = isSuppr;
	}
	
	public InfirmiereEntity() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAdresse_id() {
		return adresse_id;
	}
	public void setAdresse_id(int adresse_id) {
		this.adresse_id = adresse_id;
	}
	public int getNumeroProfessionnel() {
		return numeroProfessionnel;
	}
	public void setNumeroProfessionnel(int numeroProfessionnel) {
		this.numeroProfessionnel = numeroProfessionnel;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getTelPro() {
		return telPro;
	}
	public void setTelPro(String telPro) {
		this.telPro = telPro;
	}
	public String getTelPerso() {
		return telPerso;
	}
	public void setTelPerso(String telPerso) {
		this.telPerso = telPerso;
	}
	public String getIsSuppr() {
		return isSuppr;
	}
	public void setIsSuppr(String isSuppr) {
		this.isSuppr = isSuppr;
	}
}
