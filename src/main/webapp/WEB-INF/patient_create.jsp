<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
 <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


<title>Patient Create</title>
</head>
<body>
<header>

	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3">
		<div class="container-fluid">
			<a class="navbar-brand" href="/">Medical Art</a>
			<button
					class="navbar-toggler"
					type="button"
					data-mdb-toggle="collapse"
					data-mdb-target="#navbarToggler"
					aria-controls="navbarToggler"
					aria-expanded="false"
					aria-label="Toggle navigation"
			>
				<i class="fas fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="patients">Patients</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="infirmieres">Infirmières</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="deplacements">Déplacements</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>
<div class="container" style="display: flex;justify-content: center;">

	<form method="POST" action="patientCreate">
	<div class="mb-3">
		<label class="form-label"> Nom
			<input type="text" name="nom" class="form-control">
		</label>
	</div>
	<div class="mb-3">
		<label class="form-label"> Prenom
			<input type="text" name="prenom" class="form-control">
		</label>
	</div>
		<label class="form-label"> dateDeNaissance
			<input type="date" name="dateDeNaissance" class="form-control">
		</label>
		<div class="mb-3">
		<label class="form-label"> Sexe
			<input type="text" name="sexe" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> Numero de securité sociale
			<input type="number" name="numeroSecuriteSocial" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> choix infirmiere
			<select name="infirmiere_id" class="form-select">
				<c:forEach items="${ infirmieres }" var="infirmiere">
					<option value="${ infirmiere.id }">${infirmiere.nom} ${infirmiere.prenom}</option>
				</c:forEach>
			</select>
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> numero de rue
			<input type="number" name="numero" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<label class="form-label"> rue
			<input type="text" name="rue" class="form-control">
		</label>
		</div>
		<label class="form-label"> code postal
			<input type="number" name="cp" class="form-control">
		</label>
		<div class="mb-3">
		<label> ville
			<input type="text" name="ville" class="form-control">
		</label>
		</div>
		<div class="mb-3">
		<input type="submit" value="Submit">
		</div>
	</form>
</div>
<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.js"></script>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</body>
</html>