package controller.infirmiere;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Entity.AdresseEntity;
import Entity.InfirmiereEntity;
import model.AdresseModel;
import model.InfirmiereModel;

/**
 * Servlet implementation class InfirmiereController
 */
@WebServlet(
		urlPatterns = { "/infirmiere" }, 
		initParams = { 
				@WebInitParam(name = "Infirmiere", value = "")
		})
public class InfirmiereController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfirmiereController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Affiche une seule infirmiere pass� en parametre dans l'URL (si elle est indiqu�e comme "non supprim�")
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AdresseModel adresseModel = new AdresseModel(getServletContext());
		AdresseEntity adresse = null;
		
		InfirmiereModel infirmiereModel = new InfirmiereModel(getServletContext());
		InfirmiereEntity infirmiere;
		
		try {
			infirmiere = infirmiereModel.fetchById(Integer.parseInt(request.getParameter("id")));
			adresse = adresseModel.fetchById(infirmiere.getAdresse_id());
			request.setAttribute("infirmiere", infirmiere);
			request.setAttribute("adresse", adresse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("WEB-INF/infirmiere.jsp").forward(request, response);
	}
}
